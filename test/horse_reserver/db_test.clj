(ns horse-reserver.db-test
  (:require [clojure.test :refer :all]
            [horse-reserver.db :refer :all])
  (:import [org.joda.time LocalDate]
           [clojure.lang ExceptionInfo]))

(defn- mk-horse [name gender] {:name name :gender gender})

(def pumpkin (mk-horse "Pumpkin" :female))
(def boombird (mk-horse "Boombird" :male))
(def female-horse pumpkin)
(def male-horse boombird)

(defn- mk-reservation-request
  ([quantity periods date]
   (mk-reservation-request quantity periods date 1))
  ([quantity periods date procedure-code]
   {:quantity quantity
    :date date
    :periods periods
    :procedure-code procedure-code}))

(defn- mk-db [horses]
  (create-database (atom {:horses horses
                          :reservations {}})))

(defn- mk-one-horse-db []
  (mk-db [pumpkin]))

(deftest test-reserve
  (deftest simple-reservation
    (let [db-atom (mk-one-horse-db)
          expected-reservation-date (LocalDate/parse "2016-07-06")
          periods [:morning]
          request (mk-reservation-request 1 periods expected-reservation-date)
          reservation (reserve db-atom request)]

      (testing "Reserving one horse returns a reservation"
        (is (= [pumpkin] (:horses reservation)))
        (is (= expected-reservation-date (:date reservation)))
        (is (= periods (:periods reservation)))
        (is (:id reservation)))

      (testing "Reserving one horse updates the reservations record in the db"
        (let [db @(:db db-atom)]
          (is (= [pumpkin] (:horses db)))
          (is (= [pumpkin] (-> db
                               :reservations
                               (get expected-reservation-date)
                               :morning
                               vals
                               flatten))))))))

(deftest multi-period-reservation
    (let [snoopy (mk-horse "Snoopy" :female)
          prickly-pete (mk-horse "Prickly Pete" :male)
          reservation-date (LocalDate/now)
          request (mk-reservation-request 1 [:morning :afternoon] reservation-date)
          db (create-database (atom {:horses [pumpkin snoopy prickly-pete]
                                     :reservations {}}))
          reservation (reserve db request)]
      
      (testing "One horse is reserved for two periods"
        (is (= 1 (-> reservation :horses count)))
        (is (= (get-in @(:db db) [:reservations reservation-date :morning])
               (get-in @(:db db) [:reservations reservation-date :afternoon]))))))

(deftest test-reservation-servicable
  (let [db (mk-one-horse-db)
        request {:quantity 1
                 :date (LocalDate/parse "2016-07-06")
                 :periods [:morning]
                 :procedure-code 1}]

    (testing "When there aren't sufficient horses available for a reservation date, period, and quanity, the reservation is not servicable"
      (is (thrown-with-msg? ExceptionInfo #"Insufficient horses available" (reserve db (assoc request :quantity 2)))))

    (testing "When sufficient horses are available for a reservation request date and period, the reservation is servicable"
      (is (= 1 (-> (reserve db request) :horses count))))

    (testing "When all horses have been reserved for a reservation date and period, the reservation is not servicable"
      (is (thrown-with-msg? ExceptionInfo #"Insufficient horses available" (reserve db request))))))

(deftest test-gender-specific-reservation
  (let [db (mk-db [female-horse male-horse])
        mares-only-procedure-code 3
        request {:quantity 2
                 :date (LocalDate/parse "2016-07-06")
                 :periods [:morning]
                 :procedure-code mares-only-procedure-code}] 

    (testing "Reservation denied when there aren't sufficient horses to fill a gender-specific reservation (partial reservation not supported)"
      (is (thrown-with-msg? ExceptionInfo #"Insufficient horses available" (reserve db request))))))
