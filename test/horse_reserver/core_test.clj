(ns horse-reserver.core-test
  (:require [cheshire.core :as cheshire]
            [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [horse-reserver.handler :refer :all]
            [horse-reserver.system :as sys]
            [ring.mock.request :as mock]))

(defn- parse-body [body]
  (cheshire/parse-string (slurp body) true))

(defn- json [x] (cheshire/generate-string x))

(defn- mk-post-request [uri params]
  (-> (mock/request :post uri (json params))
      (mock/content-type "application/json")))

(defn- mk-delete-request [uri]
  (-> (mock/request :delete uri)
      (mock/content-type "application/json")))

(def one-horse-stable [{:name "Pumpkin" :gender :female}])

(defn- mk-reservation-system [horses]
  (let [system (sys/new-system {:db (atom {:horses horses})
                                :http {:server {}}})
        handler (-> system component/start-system :app :handler)
        
        reserve-fn (fn [params]
                     (let [response (-> (mk-post-request "/api/reserve" params)
                                        handler)
                           body (-> response :body parse-body)]
                       [response body]))
        cancel-fn (fn [reservation-id]
                    (let [response (-> (mk-delete-request (format "/api/reserve/%s" reservation-id))
                                        handler)
                           body (-> response :body parse-body)]
                       [response body]))]
    [reserve-fn cancel-fn]))

(deftest n-reservation-test
  (let [[reserve _] (mk-reservation-system one-horse-stable)]
    (testing "Reserving with invalid input fails"
      (let [[response body] (reserve {})]
        (is (= 400 (:status response)))
        (is (= {:errors
                {:quantity "missing-required-key"
                 :date "missing-required-key"
                 :periods "missing-required-key"
                 :procedure-code "missing-required-key"}} body))))

    (testing "Reserving one horse shows which horse is reserved"
      (let [expected-periods ["morning"]
            expected-date "2016-07-06"
            [response body] (reserve
                             {:quantity 1
                              :date expected-date
                              :periods expected-periods
                              :procedure-code 1})
            reservation (:reservation body)]
        (is (= 201 (:status response)))
        (is (= expected-periods (:periods reservation)))
        (is (= expected-date (:date reservation)))
        (is (= [{:name "Pumpkin" :gender "female"}] (:horses reservation)))
        (is (:id reservation))))))

(deftest blackout-periods
  (let [[reserve _] (mk-reservation-system one-horse-stable)
        reservation-request {:quantity 1
                             :date "2016-07-06"
                             :periods ["morning"]
                             :procedure-code 1}]
    
    (testing "Horses cannot be double-booked"
      (let [[first-response _] (reserve reservation-request)
            [second-response second-body] (reserve reservation-request)]
        (is (= 201 (:status first-response)))
        (is (= 409 (:status second-response)))
        (is (= "Insufficient horses available."
               (:message second-body)))))
    
    (testing "Horses can't be double-booked even when one period in a multi-period reservation is available"
      (let [multi-period-request (update reservation-request :periods conj "afternoon")
            [response body] (reserve multi-period-request)]
        (is (= 409 (:status response)))
        (is (= "Insufficient horses available."
               (:message body)))))))

(deftest gender-specific-procedures
  (testing "Reservation denied when an invalid procedure code is submitted"
    (let [[reserve _] (mk-reservation-system one-horse-stable)
          reservation-request {:quantity 1
                               :date "2016-07-06"
                               :periods ["morning"]
                               :procedure-code 999}
          [response body] (reserve reservation-request)]
      (is (= 400 (:status response)))
      (is (= "Invalid procedure code"
             (:message body))))))

(deftest cancel-reservations
  (testing "An existing reservation can be cancelled"
    (let [[reserve cancel] (mk-reservation-system one-horse-stable)
          reservation-request {:quantity 1
                               :date "2016-07-06"
                               :periods ["morning"]
                               :procedure-code 1}
          [response {reservation :reservation}] (reserve reservation-request)]
      (is (= 201 (:status response)) "The initial reservation is accepted")
      (is (= 409 (-> reservation-request
                     reserve
                     first
                     :status)) "A duplicate reservation fails")

      (is (= 200 (-> (cancel (:id reservation))
                     first
                     :status)) "Cancelling the reservation makes the horse available for subsequent requests")

      (is (= 201 (-> reservation-request
                     reserve 
                     first
                     :status)) "The reservation is no longer a duplicate and is successful"))))
