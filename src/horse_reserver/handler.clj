(ns horse-reserver.handler
  (:require [compojure.api.exception :as ex]
            [compojure.api.sweet :refer :all]
            [schema.core :as s]
            [ring.util.http-response :refer :all]
            [horse-reserver.schema :as hrs]
            [horse-reserver.db :as hrdb]))

(defn- error-category->response-generator [error-category]
  (case error-category
    :invalid-request bad-request
    :availability conflict
    internal-server-error))

(defn- error-handler [error {error-category :error-category :as exception-info} request]
  ((error-category->response-generator error-category) {:message (.getMessage error)}))

(defn mk-endpoints [{db :db}]
  (api
   {:exceptions {:handlers {::ex/default error-handler}}
    :swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Horse-reserver"
                    :description "API for reserving horses"}
             :tags [{:name "api"
                     :description "some apis for horse reservations"}]}}}

    (context "/api" []
      :tags ["api"]

      (POST "/reserve" []
            :summary "Reserves N-horses for the given date, period, procedure,
                      and returns the reservation request result (which either 
                      lists the horses reserved by the request or a message 
                      explaining why the reservation was rejected)."
            :body [reservation-request hrs/ReservationRequest]
            :return {:reservation hrs/Reservation}
            (created {:reservation (hrdb/reserve db reservation-request)}))

      (DELETE "/reserve/:reservation-id" []
              :summary "Cancels a reservation."
              :path-params [reservation-id]
              (hrdb/cancel db reservation-id)
              (ok {:message (str "Deleted reservation " reservation-id)})))))
