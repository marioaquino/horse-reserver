(ns horse-reserver.main
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [horse-reserver.system :refer [new-system]]))

(def config
  {:http {:port 3000}
   :db (atom {:horses [{:name "All Star" :gender :male}
                       {:name "Genesis" :gender :male}
                       {:name "Pumpkin" :gender :female}
                       {:name "Misty" :gender :female}
                       {:name "Good Morning Sunshine" :gender :female}
                       {:name "Boombird" :gender :female}
                       {:name "Sunny" :gender :female}
                       {:name "Brooke" :gender :female}]})
   :app {}})

(defn -main [& args]
  (let [system (new-system config)]
    (println "Starting HTTP server on port" (-> system :http :port))
    (component/start system)))
