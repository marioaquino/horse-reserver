(ns horse-reserver.schema
  (:require [schema.core :as s]
            [schema.coerce :as coerce])
  (:import org.joda.time.LocalDate))

(s/defschema Period
  (s/enum :morning :afternoon :evening))

(s/defschema Horse
  {:name s/Str
   :gender (s/enum :male :female)})

(s/defschema ReservationRequest
  {:quantity s/Int
   :date LocalDate
   :periods [Period]
   :procedure-code s/Int})

(s/defschema Reservation
  (-> ReservationRequest
      (dissoc :quantity :procedure-code)
      (assoc :horses [Horse])
      (assoc :reservation-id s/Str)))
