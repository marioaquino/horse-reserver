(ns horse-reserver.db
  (:require [clojure.set :refer [difference union]]
            [com.stuartsierra.component :as component]
            [schema.core :as s]
            [horse-reserver.schema :as hrs]
            [pandect.algo.sha1 :refer [sha1]]))

(defrecord Procedure [code description genders-allowed])

(def procedures
  (let [both #{:male :female}
        mares-only #{:female}
        build-procedure (fn [[procedure-code description genders-allowed]]
                          [procedure-code (->Procedure procedure-code description genders-allowed)])]
    (into {} (map build-procedure
                  [[1 "Physical exam" both]
                   [2 "hoof exam and care" both]
                   [3 "uterine biopsy" mares-only]
                   [4 "synovial fluid collection" both]
                   [5 "blood collection for transfusion" both]]))))

(defn- get-random-horses
  ([quantity horses]
   (get-random-horses quantity horses []))
  ([quantity horses accumulator]
   (if (= (count accumulator) quantity)
     accumulator
     (let [horse (rand-nth (vec horses))]
       (get-random-horses quantity
                          (difference horses #{horse})
                          (conj accumulator horse))))))

(defn- get-reserved-horses-for-period [db reservation-date period]
  (-> (get-in @db [:reservations reservation-date period] {})
      vals
      flatten))

(defn- get-subset [super sub]
  (difference (set super) (set sub)))

(defn- get-available-horses-for-period [db-atom reservation-date period]
  (get-subset (:horses @db-atom)
              (get-reserved-horses-for-period db-atom reservation-date period)))

(defn- get-reserved-horses-set [db-atom reservation-date periods]
  (-> (mapcat (partial get-reserved-horses-for-period db-atom reservation-date) periods)
      set))

(defn- horse-eligible-for-procedure? [procedure horse]
  (some #{(:gender horse)} (:genders-allowed procedure)))

(defn- availability-in-all-periods? [db-atom {:keys [date periods quantity procedure-code]}]
  (let [available-horse-sets (map (partial get-available-horses-for-period db-atom date)
                                  periods)
        procedure (get procedures procedure-code)
        filter-ineligible-horses (fn [horses]
                                   (filter (partial horse-eligible-for-procedure? procedure) horses))
        horses-eligible-for-procedure (map filter-ineligible-horses available-horse-sets)]
    (every? #(>= (count %) quantity) horses-eligible-for-procedure)))

(defn- select-horses
  [periods->reservation-id->reserved-horses all-horses reservation-request reservation-id]
  (let [periods->reservation-id->reserved-horses* (or periods->reservation-id->reserved-horses {})
        periods-for-reservation (:periods reservation-request)
        reserved-horses-for-reservation-periods (->> (select-keys periods->reservation-id->reserved-horses* periods-for-reservation)
                                                     vals          ; periods->reservation-ids->reserved-horses
                                                     (apply merge) ; {reservation-ids [horse]...}
                                                     vals          ; ([horse-1] [horse-1 horse-2])
                                                     flatten       ; (horse-1 horse-1 horse-2)
                                                     distinct)     ; (horse-1 horse-2)
        unreserved-horses (difference (set all-horses) (set reserved-horses-for-reservation-periods))
        horses-for-reservation (get-random-horses (:quantity reservation-request) unreserved-horses)
        selected-horses (reduce (fn [m period]
                                  (update m period merge {reservation-id horses-for-reservation}))
                                periods->reservation-id->reserved-horses*
                                periods-for-reservation)]
    selected-horses))

(defn- generate-id [{:keys [quantity date periods procedure-code] :as reservation-request}]
  (sha1 (str quantity date periods procedure-code (System/currentTimeMillis))))

(defn- mk-reservation [db-atom reservation-request]
  (dosync
   (let [reservation-date (:date reservation-request)
         periods (:periods reservation-request)
         all-horses (:horses @db-atom)
         initial-reserved-horses-for-periods (get-reserved-horses-set db-atom reservation-date periods)
         reservation-id (generate-id reservation-request)]
     (swap! db-atom
            update-in
            [:reservations reservation-date]
            select-horses
            all-horses
            reservation-request
            reservation-id)
     (swap! db-atom
            update
            :reservations-index
            merge
            {reservation-id [reservation-date periods]})
     {:id reservation-id
      :date reservation-date
      :periods (:periods reservation-request)
      :horses (->> (get-in @db-atom [:reservations reservation-date])
                   (reduce-kv (fn [accumulator period id->horses]
                                (concat accumulator (get id->horses reservation-id)))
                              [])
                   distinct)})))

(defn- valid-procedure? [db {procedure-code :procedure-code}]
  (get procedures procedure-code))

(defn- raise-unless [passes-check message error-category]
  (if (not passes-check)
    (throw (ex-info message {:error-category error-category}))))

(defn- validate-reservation! [db reservation-request]
  (raise-unless (valid-procedure? db reservation-request)
                "Invalid procedure code"
                :invalid-request)
  (raise-unless (availability-in-all-periods? db reservation-request)
                "Insufficient horses available."
                :availability))

(defn- cancel-reservation [db-atom reservation-id]
  (let [dissoc-reservation-id (fn [period->reservation-id->horses periods]
                                (reduce (fn [p->r-id->h period]
                                          (update p->r-id->h period dissoc reservation-id))
                                        period->reservation-id->horses
                                        periods))
        [reservation-date periods] (get-in @db-atom [:reservations-index reservation-id])]
    (swap! db-atom update-in [:reservations reservation-date] dissoc-reservation-id periods)))

(defprotocol Reserver
  (reserve [this reservation-request])
  (cancel [this reservation-id]))

(defrecord Database [db]
  component/Lifecycle Reserver
  (start [component]
    (assoc component :db db))
  (stop [component]
    (assoc component :db nil))
  (reserve [this reservation-request]
    (validate-reservation! db reservation-request)
    (mk-reservation db reservation-request))
  (cancel [this reservation-id]
    (cancel-reservation db reservation-id)))

(defn create-database [db]
  (->Database db))
