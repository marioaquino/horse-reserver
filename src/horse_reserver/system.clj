(ns horse-reserver.system
  (:require [com.stuartsierra.component :as component]
            [duct.component.endpoint :refer [endpoint-component]]
            [duct.component.handler :refer [handler-component]]
            [horse-reserver.db :refer [create-database]]
            [horse-reserver.handler :refer [mk-endpoints]]
            [ring.component.jetty :refer [jetty-server]]))

(defn new-system [{:keys [db http app] :as opts}]
  (-> (component/system-map
       :app  (handler-component app)
       :horse-reserver (endpoint-component mk-endpoints)
       :db (create-database db)
       :http (jetty-server http))
      (component/system-using
       {:http [:app]
        :app [:horse-reserver]
        :horse-reserver [:db]})))
