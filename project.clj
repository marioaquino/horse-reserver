(defproject horse-reserver "0.1.0-SNAPSHOT"
  :description "Horse reservations is all I know how to do"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-time "0.11.0"] ; required due to bug in `lein-ring uberwar`
                 [metosin/compojure-api "1.1.0"]
                 [com.stuartsierra/component "0.3.1"]
                 [prismatic/schema "1.1.1"]
                 [duct "0.6.1"]
                 [ring-jetty-component "0.3.1"]
                 [pandect "0.6.0"]]
  :main horse-reserver.main
  :uberjar-name "server.jar"
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [cheshire "5.5.0"]
                                  [ring/ring-mock "0.3.0"]]
                   :plugins [[lein-ring "0.9.7"]]}})
